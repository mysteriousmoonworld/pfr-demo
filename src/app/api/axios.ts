import axios from 'axios';

export const axiosRequest = axios.create({
  baseURL: location.origin,
  withCredentials: true
});

export const defaultInterceptorId = axiosRequest.interceptors.response.use((response) => {
  return response.data ? response.data : response;
}, (error) => {
  return Promise.reject(error);
});

export function getBaseUrl() {
  const baseURL = getConfig().baseURL || '';
  return baseURL.substr(-1) === '/' ? baseURL : baseURL + '/';
}

const config = {
  /**
   * см axios interceptors
   * установите true, если хотите полуать response от axios без изменений (по умолчанию egip.api возвращает response.data)
   */
  removeDefaultInterceptor: false,
  baseURL: location.origin,
  apiKey: '',
  withCredentials: true,
};

export type ApiConfig = typeof config;

export function setApiConfig(opts: Partial<ApiConfig>) {
  Object.assign(config, opts);

  if (config.removeDefaultInterceptor) {
    axiosRequest.interceptors.response.eject(defaultInterceptorId);
  }

  Object.assign(axiosRequest.defaults, {
    baseURL: config.baseURL,
    withCredentials: config.withCredentials
  });
}

function getConfig() {
  return config;
}
