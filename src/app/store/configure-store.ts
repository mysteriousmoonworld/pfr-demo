import { createStore, applyMiddleware, compose } from 'redux';
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';

import rootReducer from './reducer';
import rootSaga from './sagas';
import { onError } from 'app/store/modules/app/setup/on-error';

import { setupAction } from 'app/store/setup_action';
import promiseMiddleware from 'app/store/promise_middleware/middleware';

const loggerReduxMiddleware = createLogger({
  collapsed: true,
});

export function configureStore() {
  const sagaMiddleware = createSagaMiddleware({
    onError,
  });

  const middleware = [promiseMiddleware, sagaMiddleware];

  if (NODE_ENV !== 'production') {
    middleware.push(loggerReduxMiddleware);
  }

  const composeEnhancers =
    NODE_ENV !== 'production' &&
    typeof window === 'object' &&
    (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?   
      (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
      }) : compose;

  const store = createStore(
    rootReducer,
    {},
    composeEnhancers(
      applyMiddleware(...middleware),
    ),
  );

  sagaMiddleware.run(rootSaga);

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('./reducer', () => {
      const nextRootReducer = require('./reducer').default;
      store.replaceReducer(nextRootReducer);
    });
  }
  store.dispatch(setupAction());

  return {
    store,
  };
}
