import { all } from 'redux-saga/effects';

import AppEffects from 'app/store/modules/app/effects';

export default function* root() {
  yield all([
    ...AppEffects,
  ]);
}
