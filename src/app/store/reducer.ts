import { combineReducers } from 'redux';

import appReducer from 'app/store/modules/app/reducer';

export const reducerMap = {
  app: appReducer,
};

export default combineReducers(reducerMap);
