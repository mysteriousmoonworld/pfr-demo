import { factoryAction } from 'app/store/createReducer';

const APP = factoryAction('APP');

const set = APP('set');
const update = APP('update');

const APP_TYPES = {
  set,
  update,
};

export default APP_TYPES;
