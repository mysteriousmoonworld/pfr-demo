
import APP_TYPES from 'app/store/modules/app/action_types';
import { AppState } from './types';

export const setStateKey = <K extends keyof AppState>(key: K, payload: AppState[K]) => {
  return ({
    type: APP_TYPES.set,
    key,
    payload,
  });
};

export const updateState = (payload: AppState[keyof AppState]) => {
  return ({
    type: APP_TYPES.update,
    payload,
  });
};