
import createReducer from 'app/store/createReducer';

const appReducerRaw = createReducer<'app'>(
  {},
  {}
);

export default appReducerRaw;
