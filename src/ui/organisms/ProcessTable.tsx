import React from "react";
import styled from "styles";
import "App.css";

const ProcessTable = () => {
  return (
    <Table>
      <tr>
        <th></th>
        <th>ID Процесса</th>
        <th>Имя процесса</th>
        <th>Статус</th>
        <th>Дата и время создания</th>
        <th>Количество пакетов</th>
      </tr>

      <tr>
        <td>
          <input type="checkbox" />
        </td>
        <td>99182ABK</td>
        <td>Отчетность ФНС</td>
        <td>Идентификация страхователя</td>
        <td>02.02.2019</td>
        <td>100</td>
      </tr>

      <tr>
        <td>
          <input type="checkbox" />
        </td>
        <td>67659GD</td>
        <td>ЕГРН</td>
        <td>Разнесение данных</td>
        <td>02.02.2019</td>
        <td>130</td>
      </tr>

      <tr>
        <td>
          <input type="checkbox" />
        </td>
        <td>2566oRFD</td>
        <td>Отчетность ФНС</td>
        <td>Идентификация страхователя</td>
        <td>02.02.2019</td>
        <td>45</td>
      </tr>

      <tr>
        <td>
          <input type="checkbox" />
        </td>
        <td>75429GDH</td>
        <td>Отчетность ФНС</td>
        <td>Идентификация страхователя</td>
        <td>02.02.2019</td>
        <td>23</td>
      </tr>
    </Table>
  );
};

export default ProcessTable;

const Table = styled.table`
  width: 100%;
  height: 100%;
  background-color: #fff;
  border-radius: 3px;
  border: solid 2px #e0f0ff;
  border-collapse: collapse;
  th {
    border: solid 2px #e0f0ff;
  }
  td {
    border: solid 2px #e0f0ff;
    input {
      cursor: pointer;
    }
  }
`;
