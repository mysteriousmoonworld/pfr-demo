import React from "react";
import styled from "styles";
import "App.css";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

am4core.useTheme(am4themes_animated);

const Packet = () => {
  React.useEffect(() => {
    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    var chart = am4core.create("chartdiv5", am4charts.XYChart);

    var data = [];
    console.log(data);

    chart.data = [
      {
        year: "00",
        income: 20,
        expenses: 30.5
      },
      {
        year: "02",
        income: 22,
        expenses: 34.9
      },
      {
        year: "04",
        income: 30,
        expenses: 23.1
      },
      {
        year: "06",
        income: 50,
        expenses: 28.2
      },
      {
        year: "08",
        income: 40,
        expenses: 31.9
      },
      {
        year: "10",
        income: 68,
        expenses: 31.9
      },
      {
        year: "12",
        income: 71,
        expenses: 31.9
      },
      {
        year: "14",
        income: 19,
        expenses: 31.9
      },
      {
        year: "16",
        income: 32,
        expenses: 31.9
      },
      {
        year: "18",
        income: 13,
        expenses: 31.9
      }
    ];

    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.ticks.template.disabled = true;
    categoryAxis.renderer.line.opacity = 0;
    categoryAxis.renderer.grid.template.disabled = true;
    categoryAxis.renderer.minGridDistance = 40;
    categoryAxis.dataFields.category = "year";
    categoryAxis.startLocation = 0.4;
    categoryAxis.endLocation = 0.6;

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.tooltip.disabled = true;
    valueAxis.renderer.grid.template.disabled = true;
    valueAxis.renderer.line.opacity = 0;
    valueAxis.renderer.ticks.template.disabled = true;
    valueAxis.min = 0;

    var lineSeries = chart.series.push(new am4charts.LineSeries());
    lineSeries.dataFields.categoryX = "year";
    lineSeries.dataFields.valueY = "income";
    lineSeries.tooltipText = "income: {valueY.value}";
    lineSeries.fillOpacity = 0.5;
    lineSeries.strokeWidth = 2;
    lineSeries.stroke = am4core.color("#560FAF");
    lineSeries.fill = am4core.color("#DDC1FF");

    var bullet = lineSeries.bullets.push(new am4charts.CircleBullet());
    bullet.circle.radius = 6;
    bullet.circle.fill = am4core.color("#F75BDE");
    bullet.circle.strokeWidth = 3;

    lineSeries.tooltipText = "{income}";

    lineSeries.tooltip.background.cornerRadius = 20;
    lineSeries.tooltip.background.strokeOpacity = 0;
    lineSeries.tooltip.pointerOrientation = "vertical";
    lineSeries.tooltip.label.minWidth = 40;
    lineSeries.tooltip.label.minHeight = 40;
    lineSeries.tooltip.label.textAlign = "middle";
    lineSeries.tooltip.label.textValign = "middle";

    chart.cursor = new am4charts.XYCursor();
    chart.cursor.behavior = "panXY";
    chart.cursor.xAxis = categoryAxis;
    chart.cursor.snapToSeries = lineSeries;

    chart.logo.__disabled = true;
  });

  return (
    <div style={{ width: "100%", height: "100%" }}>
      <TypesTitle>Количество пакетов за последние сутки</TypesTitle>
      <div
        id="chartdiv5"
        style={{ width: "100%", height: "80%", margin: "0 auto" }}
      ></div>
    </div>
  );
};

export default Packet;

const TypesTitle = styled.p`
  text-align: left;
  font-size: 16px;
  font-weight: 200;
`;
