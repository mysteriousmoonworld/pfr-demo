import React from 'react';
import { Switch, Route } from 'react-router-dom';
import styled from 'styles';
import Home from 'ui/organisms/Home';
import Processes from 'ui/organisms/Processes';
import Reestr from 'ui/organisms/Reestr';
import Footer from 'ui/organisms/Footer';
import { sidebarWidth } from 'ui/organisms/Sidebar';
import { headerHeight } from 'ui/organisms/Header';
import { footerHeight } from 'ui/organisms/Footer';

function Main() {
  return (
    <MainBlock>
      <ContentBlock>
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route path='/processes' component={Processes}/>
          <Route path='/reestr' component={Reestr}/>
        </Switch>
      </ContentBlock>

      <Footer />
    </MainBlock>
  );
}

export default Main;

const MainBlock = styled.div`
  position: relative;
  display: inline-block;
  height: calc(100vh - ${headerHeight}px);
  width: calc(100vw - ${sidebarWidth}px);
  vertical-align: top;
`;

const ContentBlock = styled.div`
  background: #F2F5F8;
  margin-left: 30px;
  border-top-left-radius: 30px;
  border-bottom-left-radius: 30px;
  height: calc(100% - ${footerHeight}px);
  width: calc(100% - 32px);
  padding-top: 30px;
`;