import React from "react";
import styled from "styles";
import "App.css";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

am4core.useTheme(am4themes_animated);

const ErrorsWidget = () => {
  React.useEffect(() => {
    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    var chart = am4core.create("chartdiv4", am4charts.XYChart);

    // Add data
    chart.data = [
      {
        country: "01.02.20",
        visits: 3025
      },
      {
        country: "02.02.20",
        visits: 4882
      },
      {
        country: "03.02.20",
        visits: 2809
      },
      {
        country: "04.02.20",
        visits: 3322
      },
      {
        country: "05.02.20",
        visits: 2122
      }
    ];

    // Create axes
    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "country";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 30;

    categoryAxis.tooltip.disabled = true;
    categoryAxis.renderer.minHeight = 110;

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.renderer.minWidth = 50;

    // Create series
    var series = chart.series.push(new am4charts.ColumnSeries());
    series.sequencedInterpolation = true;
    series.dataFields.valueY = "visits";
    series.dataFields.categoryX = "country";
    series.columns.template.strokeWidth = 0;

    series.columns.template.column.cornerRadiusTopLeft = 20;
    series.columns.template.column.cornerRadiusTopRight = 20;
    series.columns.template.column.fillOpacity = 0.8;

    let gradient = new am4core.LinearGradient();
    gradient.addColor(am4core.color("#FF5757"));
    gradient.addColor(am4core.color("#560FAF"));
    series.columns.template.column.fill = gradient;
    gradient.rotation = 90;

    chart.logo.__disabled = true;
  });

  return (
    <div
      style={{
        width: "100%",
        height: "100%",
        position: "absolute",
        top: "10px"
      }}
    >
      <TypesTitle>
        Статистика по ошибкам <br />{" "}
        <span>Всего получено сообщений об ошибках: 2097</span>
      </TypesTitle>
      <div
        id="chartdiv4"
        style={{ width: "90%", height: "90%", margin: "0 auto" }}
      ></div>
    </div>
  );
};

export default ErrorsWidget;

const TypesTitle = styled.p`
  margin-left: 40px;
  text-align: left;
  font-weight: 600;
  font-size: 22px;
  span {
    font-size: 16px;
    font-weight: 200;
  }
`;
