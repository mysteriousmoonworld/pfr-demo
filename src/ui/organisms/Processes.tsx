import React from 'react';
import styled from 'styles';;
import Packet from './Packet';
import ProcessTable from './ProcessTable';
import downloadIcon from 'static/img/tableDownload.png';
import tableFilter from 'static/img/tableFilter.png';
import tableSettings from 'static/img/tableSettings.png';

function Processes() {
  return (
    <React.Fragment>
      <Title>Процессы</Title>
      <RowOne>
        <Packet />
      </RowOne>

      <RowTwo>
        <TableSettings>
          <img src={downloadIcon} alt='downloadIcon' />
          <img src={tableFilter} alt='tableFilter' />
          <img src={tableSettings} alt='tableSettings' />
        </TableSettings>
        <ProcessTable />
      </RowTwo>
    </React.Fragment>
  );
}

export default Processes;

const Title = styled.h2`
  text-align: left;
  margin-left: 30px;
  font-weight: 700;
  font-size: 21px;
  color: #404A54;
  position: relative;
  top: -23px;
  margin-bottom: 0;
`;

const RowOne = styled.div`
  width: 96%;
  height: 30%;
  margin: 0 auto;
`;

const RowTwo = styled.div`
  width: 96%;
  height: 46%;
  margin: 0 auto;
  margin-top: 10px;
`;

const TableSettings = styled.div`
text-align: right;
padding-right: 30px;
margin-bottom: 10px;
cursor: pointer;
img{
  width: 15px;
  margin-left: 20px;
}
`;
