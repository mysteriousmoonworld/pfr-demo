import React from 'react';
import styled from 'styles';
import speakIcon from 'static/img/speak.png';

function Footer() {
  return (
    <FooterBlock>
      <FooterInfo>
        <Phone>8 800 555 55 55</Phone>
        <img src={speakIcon} alt='message' />
      </FooterInfo>
    </FooterBlock>
  );
}

export default Footer;

export const footerHeight = 60;

const FooterBlock = styled.div`
	position: absolute;
	background: #F2F5F8;
	bottom: 0;
	height: ${footerHeight}px;
	width: calc(100% - 2px);
`;

const FooterInfo = styled.div`
	position: relative;
	height: 100%;
	width: 97%;
	background: #fff;
	border-top-right-radius: 30px;
	border-bottom-right-radius: 30px;
	img{
		width: 20px;
		position: absolute;
		top: 50%;
		transform: translateY(-50%);
		right: 30px;
		cursor: pointer;
	}
`;

const Phone = styled.div`
	position: absolute;
	left: 30px;
	top: 50%;
	transform: translateY(-50%);
	color: #404A54;
	font-size: 14px;
`;