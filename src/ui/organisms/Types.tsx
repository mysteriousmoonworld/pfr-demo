import React from 'react';
import styled from 'styles';
import 'App.css';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

am4core.useTheme(am4themes_animated);

const Types = () => {
  React.useEffect(() => {
    // Create chart instance
    var chart = am4core.create("chartdiv", am4charts.PieChart);

    // Add data
    chart.data = [
      {
        country: "№ 1",
        litres: 25,
        color: am4core.color("#A067E8")
      },
      {
        country: "№ 2",
        litres: 25,
        color: am4core.color("#F75BDE")
      },
      {
        country: "№ 3",
        litres: 50,
        color: am4core.color("#560FAF")
      }
    ];

    // Add and configure Series
    var pieSeries = chart.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = "litres";
    pieSeries.dataFields.category = "country";
    pieSeries.slices.template.propertyFields.fill = "color";
    pieSeries.slices.template.cursorOverStyle =
      am4core.MouseCursorStyle.pointer;

    // Let's cut a hole in our Pie chart the size of 40% the radius
    chart.innerRadius = am4core.percent(40);

    // Disable ticks and labels
    pieSeries.labels.template.disabled = false;
    pieSeries.ticks.template.disabled = true;

    pieSeries.alignLabels = false;
    pieSeries.labels.template.text = "{value.percent.formatNumber('#')}%";
    pieSeries.labels.template.radius = am4core.percent(-25);
    pieSeries.labels.template.fill = am4core.color("white");

    // Disable tooltips
    pieSeries.slices.template.tooltipText = "";

    chart.logo.__disabled = true;
  });

  return (
    <div
      style={{
        width: "100%",
        height: "100%",
        position: "absolute",
        top: "10px"
      }}
    >
      <TypesTitle>
        Статистика по видам <br /> отклонений
      </TypesTitle>
      <div
        id="chartdiv"
        style={{
          width: "70%",
          height: "70%",
          margin: "0 auto",
          marginTop: "-30px"
        }}
      ></div>
    </div>
  );
};

export default Types;

const TypesTitle = styled.p`
  font-weight: 600;
  font-size: 22px;
`;
