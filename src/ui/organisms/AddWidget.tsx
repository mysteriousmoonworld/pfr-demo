import React from 'react';
import styled from 'styles';
import plusIcon from 'static/img/plus.png';

function AddWidget() {
  return (
    <PlusBlock>
      <img src={plusIcon} alt='add' />
    </PlusBlock>
  );
}

export default AddWidget;

const PlusBlock = styled.div`
	position: relative;
  	width: 100%;
	height: 92%;
	background-color: #fff;
	margin: 0 auto;
	top: 50%;
	transform: translateY(-50%);
	border-radius: 20px;
	box-shadow: 0px 3px 10px 1px rgba(0,0,0,0.1);
	img{
		position: relative;
		top: 50%;
		transform: translateY(-50%);
		width: 122px;
		height: 122px;
	}
`;
