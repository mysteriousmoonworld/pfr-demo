import React from 'react';
import styled from 'styles';
import { headerHeight } from 'ui/organisms/Header';
import 'App.css';
import Main from 'ui/organisms/Main';
import Sidebar from 'ui/organisms/Sidebar';

function Content() {
  return (
    <ContentBlock>
      <Sidebar />
      <Main />
    </ContentBlock>
  );
}

export default Content;

const ContentBlock = styled.div`
  height: calc(100vh - ${headerHeight}px);
`;