import React from 'react';
import styled from 'styles';;
import WidgetWrap from './WidgetWrap';
import AddWidget from './AddWidget';
import Types from './Types';
import Term from './Term';
import Fns from './Fns';
import ErrorsWidget from './ErrorsWidget';

function Home() {
  return (
    <React.Fragment>
      <RowOne>
        <LeftSide>
          <FirsChart>
            <WidgetWrap />
            <Types />
          </FirsChart>

          <SecondChart>
            <WidgetWrap />
            <Term />
          </SecondChart>
        </LeftSide>

        <RightSide>
          <WidgetWrap />
          <Fns />
        </RightSide>
      </RowOne>

      <RowTwo>
        <LeftSideTwo>
          <WidgetWrap />
          <ErrorsWidget />
        </LeftSideTwo>

        <RightSide>
          <FirsChart>
            <AddWidget />
          </FirsChart>

          <SecondChart />
        </RightSide>
      </RowTwo>
    </React.Fragment>
  );
}

export default Home;

const RowOne = styled.div`
  width: 96%;
  height: 42%;
  margin: 0 auto;
`;

const RowTwo = styled.div`
  width: 96%;
  height: 42%;
  margin: 0 auto;
  margin-top: 40px;
`;

const LeftSide = styled.div`
  position: relative;
  vertical-align: top;
  width: 49%;
  height: 100%;
  display: inline-block;
  padding: 0 10px;
`;

const LeftSideTwo = styled(LeftSide)`
  width: 49%;
`;

const RightSide = styled.div`
  position: relative;
  vertical-align: top;
  width: 47%;
  height: 100%;
  display: inline-block;
  padding: 0 10px;
`;

const FirsChart = styled.div`
  position: relative;
  width: 48.5%;
  height: 100%;
  display: inline-block;
  padding-right: 5px;
`;

const SecondChart = styled.div`
  position: relative;
  width: 50%;
  height: 100%;
  display: inline-block;
  vertical-align: top;
  padding-left: 5px;
`;
