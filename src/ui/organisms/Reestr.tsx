import React from 'react';
import styled from 'styles';
import ReestrTable from './ReestrTable';
import tableFilter from 'static/img/tableFilter.png';
import tableSettings from 'static/img/tableSettings.png';
import searchIcon from 'static/img/searchIcon.png';
import dataIcon from 'static/img/data.png';

function Reestr() {
  return (
    <React.Fragment>
      <Title>Реестр пакетов</Title>
      <RowOne>
        <SearchWrap>
          <SearchBlock>
            <Search />
            <img src={searchIcon} alt="search" />
            <SearchData placeholder="02.02.2019 - 03.03.2019" />
            <img src={dataIcon} alt="dataIcon" />
          </SearchBlock>

          <TableSettings>
            <img src={tableFilter} alt="tableFilter" />
            <img src={tableSettings} alt="tableSettings" />
          </TableSettings>
        </SearchWrap>

        <ReestrTable />
      </RowOne>
    </React.Fragment>
  );
}

export default Reestr;

const Title = styled.h2`
  text-align: left;
  margin-left: 30px;
  font-weight: 700;
  font-size: 21px;
  color: #404a54;
  position: relative;
  top: -23px;
  margin-bottom: 0;
`;

const RowOne = styled.div`
  width: 96%;
  height: 80%;
  margin: 0 auto;
  margin-top: 30px;
`;

const SearchWrap = styled.div`
  padding-right: 30px;
  margin-bottom: 20px;
  text-align: left;
`;

const TableSettings = styled.div`
  display: inline-block;
  width: 30%;
  text-align: right;
  cursor: pointer;
  img {
    width: 15px;
    margin-left: 20px;
    position: relative;
    top: 3px;
  }
`;

const SearchBlock = styled.div`
  display: inline-block;
  width: 70%;
  img {
    width: 16px;
    height: 16px;
    position: relative;
    right: 25px;
    top: 3px;
    cursor: pointer;
  }
`;

const Search = styled.input`
  width: 45%;
  height: 32px;
  border: none;
  background-color: #fff;
  border: 1px solid #e0f0ff;
  border-radius: 2px;
  padding-left: 15px;
  &:focus {
    outline: none;
  }
`;

const SearchData = styled.input`
  width: 25%;
  height: 32px;
  border: none;
  background-color: #fff;
  border: 1px solid #e0f0ff;
  border-radius: 2px;
  padding-left: 15px;
  ::placeholder {
    color: #560faf;
    font-size: 13px;
  }
  &:focus {
    outline: none;
  }
`;
