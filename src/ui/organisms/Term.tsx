import React from 'react';
import styled from 'styles';;
import 'App.css';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

am4core.useTheme(am4themes_animated);

const Term = () => {

  React.useEffect(() => {
    // Create chart instance
    var chart = am4core.create("chartdiv2", am4charts.PieChart);

    // Add data
    chart.data = [{
      "country": "№ 1",
      "litres": 80,
      "color": am4core.color("#FF5757")
    }, {
      "country": "№ 2",
      "litres": 20,
      "color": am4core.color("#560FAF")
    }];

    // Add and configure Series
    var pieSeries = chart.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = "litres";
    pieSeries.dataFields.category = "country";
    pieSeries.slices.template.propertyFields.fill = "color";

    // Let's cut a hole in our Pie chart the size of 40% the radius
    chart.innerRadius = am4core.percent(55);

    // Disable ticks and labels
    pieSeries.labels.template.disabled = true;
    pieSeries.ticks.template.disabled = true;

    // Disable tooltips
    pieSeries.slices.template.tooltipText = "";

    var label = chart.seriesContainer.createChild(am4core.Label);
    label.text = "80%";
    label.horizontalCenter = "middle";
    label.verticalCenter = "middle";
    label.fontSize = 27;

    chart.logo.__disabled = true;

  });
 
    return (
      <div style={{ width: "100%", height: "100%", position: 'absolute', top: '10px' }}>
        <TypesTitle>Превышен срок ожидания</TypesTitle>
        <div id="chartdiv2" style={{ width: "60%", height: "60%", margin: '0 auto', marginTop: '-30px' }}></div>
        <Description>Пакетов с превышенным сроком ожидания - <span>123888</span></Description>
				<DescriptionOne>Пакетов без превышения срока ожидания - <span>87600</span></DescriptionOne>
      </div>
    );
  
}

export default Term;

const TypesTitle = styled.p`
  font-weight: 600;
  font-size: 22px;
`;

const Description = styled.p`
	width: 96%;
	font-size: 12px;
	span{
		color: #FF5757;
	}
`;

const DescriptionOne = styled.p`
	width: 96%;
	font-size: 12px;
	span{
		color: #560FAF;
	}
`;