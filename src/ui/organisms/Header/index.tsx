import React from 'react';
import styled from 'styles';
import logoImg from 'static/img/logo.png';
import searchIcon from 'static/img/searchIcon.png';
import notificationsIcon from 'static/img/bell.png';
import userIcon from 'static/img/userIcon.png';

function Header() {
  return (
    <HeaderBlock>
      <LogoBlock>
        <img src={logoImg} alt="logo" />
        <Discription>
          Мониторинг <br /> и УТП
        </Discription>
      </LogoBlock>

      <InteractionsBlock>
        <SearchBlock>
          <Search />
          <img src={searchIcon} alt="search" />
        </SearchBlock>

        <NotificationsBlock>
          <img src={notificationsIcon} alt="notifications" />
        </NotificationsBlock>

        <UserBlock>
          <img src={userIcon} alt="user" />
        </UserBlock>
      </InteractionsBlock>
    </HeaderBlock>
  );
}

export default Header;

export const headerHeight = 64;

const HeaderBlock = styled.div`
  height: ${headerHeight}px;
`;

const LogoBlock = styled.div`
  display: inline-block;
  width: 200px;
  height: 100%;
  vertical-align: top;
  cursor: pointer;
  img {
    width: 40px;
    margin-top: 18px;
    margin-left: 25px;
    margin-right: 10px;
  }
`;

const Discription = styled.div`
  display: inline-block;
  text-align: left;
  position: relative;
  top: 2px;
`;

const InteractionsBlock = styled.div`
  display: inline-block;
  width: calc(100% - 200px);
  height: 100%;
`;

const SearchBlock = styled.div`
  display: inline-block;
  height: 100%;
  width: 88%;
  vertical-align: top;
  img {
    width: 16px;
    height: 16px;
    position: relative;
    right: 10px;
    top: 18px;
    cursor: pointer;
  }
`;

const NotificationsBlock = styled.div`
  display: inline-block;
  height: 100%;
  width: 5%;
  vertical-align: top;
  cursor: pointer;
  img {
    width: 24px;
    height: 26px;
    position: relative;
    top: 50%;
    transform: translateY(-50%);
  }
`;

const UserBlock = styled.div`
  display: inline-block;
  height: 100%;
  width: 5%;
  cursor: pointer;
  img {
    width: 34px;
    height: 34px;
    position: relative;
    top: 50%;
    transform: translateY(-50%);
  }
`;

const Search = styled.input`
  width: 95%;
  height: 32px;
  border-radius: 20px;
  border: none;
  background-color: #f2f5f8;
  position: relative;
  top: 50%;
  transform: translateY(-50%);
  left: 26px;
  &:focus {
    outline: none;
  }
`;
