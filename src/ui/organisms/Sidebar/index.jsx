import React from 'react';
import { Link } from 'react-router-dom'
import styled, { css } from 'styles';
import settingsIcon from 'static/img/settings.png';
import dashboard from 'static/img/dashboard.png';
import process from 'static/img/process.png';
import reestr from 'static/img/reestr.png';
import dashboardWhite from 'static/img/dashboardWhite.png';
import processWhite from 'static/img/processWhite.png';
import reestrWhite from 'static/img/reestrWhite.png';
import 'App.css';
import { useRouter } from 'hooks';


const HeaderComponent = React.memo(() => {
	const router = useRouter();
	return (
		<SideBarContainer >
			<MenuList>
				<ListItem isActive={router.pathname === '/'}>
					<TopRadiusBlock isActive={router.pathname === '/'}>
						<TopRadius />
					</TopRadiusBlock>
					
					<DashboardLinkImg linkIcon={router.pathname === '/' ? dashboard : dashboardWhite}/>
					<Link to='/' isActive={router.pathname === '/'} style={{ display: 'inline-block', position: 'relative', top: '-7px', left: '10px'}}>Дашборд</Link>

					<BottomRadiusBlock isActive={router.pathname === '/'}>
						<BottomRadius />
					</BottomRadiusBlock>
				</ListItem>

				<ListItem isActive={router.pathname === '/processes'}>
					<TopRadiusBlock isActive={ router.pathname === '/processes'}>
						<TopRadius />
					</TopRadiusBlock>

					<ProcessLinkImg linkIcon={router.pathname === '/processes' ? process : processWhite}/>
					<Link to='/processes' isActive={router.pathname === '/processes'} style={{ display: 'inline-block', position: 'relative', top: '-7px', left: '10px'}}>Процессы </Link>

					<BottomRadiusBlock isActive={router.pathname === '/processes'}>
						<BottomRadius />
					</BottomRadiusBlock>
				</ListItem>

				<ListItem isActive={router.pathname === '/reestr'}>
					<TopRadiusBlock isActive={router.pathname === '/reestr'}>
						<TopRadius />
					</TopRadiusBlock>

					<ReestrLinkImg linkIcon={router.pathname === '/reestr' ? reestr : reestrWhite}/>
					<Link to='/reestr' isActive={router.pathname === '/reestr'} style={{ display: 'inline-block', position: 'relative', top: '-1px', left: '10px'}}>Реестр <br /> пакетов</Link>

					<BottomRadiusBlock isActive={router.pathname === '/reestr'}>
						<BottomRadius />
					</BottomRadiusBlock>
				</ListItem>
			</MenuList>

			<SettingsBlock>
				<img src={settingsIcon} alt='settings'/>
				<p>Настройки</p>
			</SettingsBlock>
		</SideBarContainer> 
	);
})

export default HeaderComponent;

export const sidebarWidth = 200;

const SideBarContainer = styled.div`
	display: inline-block;
	height: 100%;
	width: ${sidebarWidth}px;
	background: #560FAF;
	border-top-right-radius: 30px;
`;

const MenuList = styled.ul`
	position: relative;
	top: 70px;
	padding-left: 16px;
`;


const ListItem = styled.li`
	${(props) => props.isActive ? css`background-color: #fff;` : null};
	position: relative;
	list-style-type: none;
	border-top-left-radius: 30px;
	border-bottom-left-radius: 30px;
	padding: 20px;
	text-align: left;
	a{
		${(props) => props.isActive ? css`color: #560FAF;` : css`color: #fff;`};
		text-decoration: none;
		font-size: 17px;
	}
`;

const TopRadiusBlock = styled.div`
	${(props) => props.isActive ? css`display: block;` : css`display: none;`};
	position: absolute;
	right: 0;
  	top: -20px;
	background-color: #fff;
	width: 20px; 
	height: 20px; 
`;

const TopRadius = styled.div`
	width: 20px; 
	height: 20px; 
	background: #560FAF;
	border-radius: 0 0 100%;
`;

const BottomRadiusBlock = styled.div`
	${(props) => props.isActive ? css`display: block;` : css`display: none;`};
	position: absolute;
	right: 0;
  	bottom: -20px;
	background-color: #fff;
	width: 20px; 
	height: 20px;
	transform: rotate(90deg); 
`;

const BottomRadius = styled.div`
	width: 20px; 
	height: 20px; 
	background: #560FAF;
	border-radius: 100% 0 0;
`;

const DashboardLinkImg = styled.div`
	content: ${(props) => `url(${props.linkIcon})`};
	width: 30px;
	height: 30px;
	display: inline-block;
	position: relative;
  	top: 6px;	
`;

const ProcessLinkImg = DashboardLinkImg;

const ReestrLinkImg = styled(DashboardLinkImg)`
	top: 2px;
`;

const SettingsBlock = styled.div`
	position: absolute;
	bottom: 17px;
	img{
		display: inline-block;
		width: 20px;
		height: 20px;
		margin-left: 40px;
		margin-right: 14px;
		&:hover{
			cursor: pointer;
		}
	}
	p{
		position: relative;
		top: -4px;
		display: inline-block;
		color: #fff;
		&:hover{
			cursor: pointer;
		}
	}
`;