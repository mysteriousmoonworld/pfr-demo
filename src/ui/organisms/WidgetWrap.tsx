import React from 'react';
import styled from 'styles';

function WidgetWrap() {
  return (
    <WrapBlock>
			<Circle />
		</WrapBlock>
  );
}

export default WidgetWrap;

const WrapBlock = styled.div`
	position: relative;
  	width: 100%;
	height: 92%;
	background-color: #fff;
	margin: 0 auto;
	top: 50%;
	transform: translateY(-50%);
	border-radius: 20px;
	box-shadow: 0px 3px 10px 1px rgba(0,0,0,0.1);
	overflow: hidden;
`;

const Circle = styled.div`
  	top: 55%;
	background-color: #F2F5F8;
	width: 100%;
	height: 100%;
	border-radius: 50%;
	position: absolute;
`;
