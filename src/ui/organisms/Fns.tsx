import React from "react";
import styled from "styles";
import "App.css";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

am4core.useTheme(am4themes_animated);

const Fns = () => {
  React.useEffect(() => {
    // Themes begin
    am4core.useTheme(am4themes_animated);
    // Themes end

    // Create chart instance
    var chart = am4core.create("chartdiv3", am4charts.XYChart);

    // Add data
    chart.data = [
      {
        date: "2013-01-14",
        value: 70
      },
      {
        date: "2013-01-15",
        value: 73
      },
      {
        date: "2013-01-16",
        value: 71
      },
      {
        date: "2013-01-17",
        value: 74
      },
      {
        date: "2013-01-18",
        value: 78
      },
      {
        date: "2013-01-19",
        value: 85
      },
      {
        date: "2013-01-20",
        value: 82
      },
      {
        date: "2013-01-21",
        value: 83
      },
      {
        date: "2013-01-22",
        value: 88
      },
      {
        date: "2013-01-23",
        value: 85
      },
      {
        date: "2013-01-24",
        value: 85
      },
      {
        date: "2013-01-25",
        value: 80
      },
      {
        date: "2013-01-26",
        value: 87
      },
      {
        date: "2013-01-27",
        value: 84
      },
      {
        date: "2013-01-28",
        value: 83
      },
      {
        date: "2013-01-29",
        value: 84
      },
      {
        date: "2013-01-30",
        value: 81
      }
    ];

    // Set input format for the dates
    chart.dateFormatter.inputDateFormat = "yyyy-MM-dd";

    // Create axes
    var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

    console.log(valueAxis, dateAxis);

    // Create series
    var series = chart.series.push(new am4charts.LineSeries());
    series.dataFields.valueY = "value";
    series.dataFields.dateX = "date";
    series.tooltipText = "{value}";
    series.strokeWidth = 2;
    series.minBulletDistance = 15;
    series.stroke = am4core.color("#560FAF");
    series.fill = am4core.color("#560FAF");
    series.tooltipText = "{value}";

    series.tooltip.background.cornerRadius = 20;
    series.tooltip.background.strokeOpacity = 0;
    series.tooltip.pointerOrientation = "vertical";
    series.tooltip.label.minWidth = 40;
    series.tooltip.label.minHeight = 40;
    series.tooltip.label.textAlign = "middle";
    series.tooltip.label.textValign = "middle";

    chart.cursor = new am4charts.XYCursor();
    chart.cursor.behavior = "panXY";
    chart.cursor.xAxis = dateAxis;
    chart.cursor.snapToSeries = series;

    // Make bullets grow on hover
    var bullet = series.bullets.push(new am4charts.CircleBullet());
    bullet.circle.strokeWidth = 2;
    bullet.circle.radius = 4;
    bullet.circle.fill = am4core.color("#FF5757");

    chart.logo.__disabled = true;
  });

  return (
    <div
      style={{
        width: "100%",
        height: "100%",
        position: "absolute",
        top: "10px"
      }}
    >
      <TypesTitle>
        Статистика за период, отчетность ФНС <br />{" "}
        <span>01.01.2018 - 01.10.2018</span>
      </TypesTitle>
      <div
        id="chartdiv3"
        style={{ width: "90%", height: "60%", margin: "0 auto" }}
      ></div>
    </div>
  );
};

export default Fns;

const TypesTitle = styled.p`
  margin-left: 40px;
  text-align: left;
  font-weight: 600;
  font-size: 22px;
  span {
    font-size: 16px;
    font-weight: 200;
  }
`;

// const Description = styled.p`
// 	width: 96%;
// 	font-size: 12px;
// 	span{
// 		color: #FF5757;
// 	}
// `;

// const DescriptionOne = styled.p`
// 	width: 96%;
// 	font-size: 12px;
// 	span{
// 		color: #560FAF;
// 	}
// `;
