import React from 'react';
import './App.css';
import Header from './ui/organisms/Header'
import Content from './ui/organisms/Content';

function App() {
  return (
    <div className="App">
      <Header />
      <Content />
    </div>
  );
}

export default App;
