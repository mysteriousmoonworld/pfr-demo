import * as React from 'react';
import * as ReactDOM  from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider as ReduxProvider } from 'react-redux';
import { configureStore } from 'app/store/configure-store';

const AppContainer = React.lazy(() => (
  import(/* webpackChunkName: "AppContainer" */ './App')
));

const storeInit = configureStore();

ReactDOM.render(
  <ReduxProvider store={storeInit.store}>
    <React.Suspense fallback={<div></div>}>
      <BrowserRouter>
        <AppContainer />
      </BrowserRouter>
    </React.Suspense>
  </ReduxProvider>,
  document.querySelector('#root'),
);
