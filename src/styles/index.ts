
// styled-components.ts
import * as styledComponents from 'styled-components';
import { IStyledComponentsTheme } from 'styles/themes/types';

const {
  default: styled,
  css,
  createGlobalStyle,
  keyframes,
  ThemeProvider,
  ThemeContext,
} = styledComponents as styledComponents.ThemedStyledComponentsModule<IStyledComponentsTheme>;

export type ThemedStyledProps<P> = styledComponents.ThemedStyledProps<P, IStyledComponentsTheme>;

export {
  css,
  createGlobalStyle,
  keyframes,
  ThemeProvider,
  ThemeContext,
};

export default styled;